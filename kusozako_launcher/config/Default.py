# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


DEFAULT = """
{
    "width": 400,
    "height": 600,
    "foreground-color": "White",
    "background-color": "alpha(Blue, 0.5)",
    "background-image": "/path/to/image",
    "include-directories": [],
    "ignore-directories": [],
    "opacity": 0.5
}
"""
