# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.config.Config import KusozakoConfig
from kusozako_launcher.css.Css import KusozakoCss
from .application.Application import DeltaApplication


class DeltaMainLoop(DeltaEntity):

    def __init__(self):
        self._parent = None
        KusozakoConfig.init()
        KusozakoCss()
        application = DeltaApplication(self)
        exit_status = application.run(sys.argv)
        sys.exit(exit_status)
