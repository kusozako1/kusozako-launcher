# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

UP_KEY_PRESSED = "up-key-pressed"           # None
DOWN_KEY_PRESSED = "down-key-pressed"       # None
ENTRY_CHANGED = "entry-changed"             # keyword as str
ENTRY_ACTIVATED = "entry-activated"         # None
APP_INFO_LAUNCHED = "app-info-launched"     # Gio.AppInfo
