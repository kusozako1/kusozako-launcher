# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GICON = 0                   # Gio.Icon
DESCRIPTION = 1             # str
APP_INFO = 2                # Gio.AppInfo
KEYWORDS = 3                # str
PRIORITY = 4                # int
