# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib
from kusozako_launcher.const import ConfigKeys
from kusozako_launcher.config.Config import KusozakoConfig
from .BackgroundImage import KusozakoBackgroundImage

TARGETS = {
    ConfigKeys.FOREGROUND_COLOR: "/*KUSOZAKO_FOREGROUND_COLOR*/",
    ConfigKeys.BACKGROUND_COLOR: "/*KUSOZAKO_BACKGROUND_COLOR*/"
}

TEMPLATES = {
    ConfigKeys.FOREGROUND_COLOR: "color: {};",
    ConfigKeys.BACKGROUND_COLOR: "background-color: {};"
}


class KusozakoCss:

    def _get_raw_data(self):
        names = [GLib.path_get_dirname(__file__), "application.css"]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_.decode("utf-8")

    def _get_css_data(self):
        raw_data = self._get_raw_data()
        background_image = KusozakoBackgroundImage()
        raw_data = background_image.replace(raw_data)
        config = KusozakoConfig.get_default()
        for key, target in TARGETS.items():
            value = config[key]
            if value is None:
                continue
            template = TEMPLATES[key]
            replacement = template.format(value)
            raw_data = raw_data.replace(target, replacement)
        return bytes(raw_data, "utf-8")

    def __init__(self):
        css_provider = Gtk.CssProvider()
        css_data = self._get_css_data()
        css_provider.load_from_data(css_data)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
            )
