# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GSound', '1.0')
gi.require_version('GtkLayerShell', '0.1')

APPLICATION_ID = "com.gitlab.kusozako1.Launcher"
VERSION = "2024.09.26"
