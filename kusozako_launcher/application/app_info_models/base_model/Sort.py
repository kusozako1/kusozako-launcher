# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.const import AppInfoColumns

SORT_COLUMN_ID = AppInfoColumns.DESCRIPTION


class FoxtrotSort:

    def _compare_priority(self, model, alfa, bravo):
        alfa_priority = model[alfa][AppInfoColumns.PRIORITY]
        bravo_priority = model[bravo][AppInfoColumns.PRIORITY]
        if alfa_priority == bravo_priority:
            return 0
        return -1 if alfa_priority > bravo_priority else 1

    def _sort_func(self, model, alfa, bravo, user_data=None):
        result = self._compare_priority(model, alfa, bravo)
        if result != 0:
            return result
        alfa_description = model[alfa][SORT_COLUMN_ID]
        bravo_descripton = model[bravo][SORT_COLUMN_ID]
        return 1 if alfa_description >= bravo_descripton else -1

    def __init__(self, model):
        model.set_sort_column_id(SORT_COLUMN_ID, Gtk.SortType.ASCENDING)
        model.set_sort_func(SORT_COLUMN_ID, self._sort_func)
