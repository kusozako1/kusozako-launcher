# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_launcher.Entity import DeltaEntity
from .CommandlineParser import FoxtrotCommandlineParser
from .priority_cache.PriorityCache import DeltaPriorityCache


class DeltaBuilder(DeltaEntity):

    def _get_gicon(self, app_info):
        gicon = app_info.get_icon()
        if gicon is None:
            gicon = Gio.Icon.new_for_string("application-x-executable")
        return gicon

    def _build(self):
        for app_info in Gio.AppInfo.get_all():
            gicon = self._get_gicon(app_info)
            description = self._commandline_parser.get_description(app_info)
            name = app_info.get_name()
            supported_types = app_info.get_supported_types()
            keywords = description+name+str(supported_types)
            priority = self._priority.get_priority(name)
            user_data = [gicon, description, app_info, keywords, priority]
            self._raise("delta > append", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._commandline_parser = FoxtrotCommandlineParser()
        self._priority = DeltaPriorityCache(self)
        self._build()
