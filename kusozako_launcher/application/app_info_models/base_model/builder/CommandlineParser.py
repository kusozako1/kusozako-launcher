# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


class FoxtrotCommandlineParser:

    def _is_option(self, element):
        for option in ("%", "-"):
            if element.startswith(option):
                return True
        return False

    def _is_optional_element(self, element):
        if ">" in element:
            return True
        return self._is_option(element)

    def _get_body_from_commandline(self, app_info, commandline):
        for element in reversed(commandline.split(" ")):
            if self._is_optional_element(element):
                continue
            return element.split("/")[-1].replace('"', "")
        # just for fail safe
        return app_info.get_name()

    def _parse(self, app_info):
        commandline = app_info.get_commandline()
        if commandline is None:
            return app_info.get_name()
        return self._get_body_from_commandline(app_info, commandline)

    def get_description(self, app_info):
        command = self._parse(app_info)
        comment = app_info.get_string("Comment")
        if comment is None or comment == "":
            comment = app_info.get_display_name()
        return "{} ({})".format(command, comment)
