# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .AppInfoLaunched import DeltaAppInfoLaunched


class EchoObservers:

    def __init__(self, parent):
        DeltaAppInfoLaunched(parent)
