# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.const import LauncherSignals
from kusozako_launcher.alfa.LauncherObserver import AlfaLauncherObserver


class DeltaAppInfoLaunched(AlfaLauncherObserver):

    __signal__ = LauncherSignals.APP_INFO_LAUNCHED

    def _signal_received(self, app_info):
        self._raise("delta > app info launched", app_info)
