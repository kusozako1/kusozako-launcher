# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class DeltaActivate(DeltaEntity):

    def _on_event(self, application):
        application.hold()

    def __init__(self, parent):
        self._parent = parent
        application = self._enquiry("delta > application")
        application.connect("activate", self._on_event)
