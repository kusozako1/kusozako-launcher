# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_launcher import VERSION
from kusozako_launcher.Entity import DeltaEntity

INIT_OPTION = (
    "init",
    ord("i"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.NONE,
    "initialize kusozako-launcher",
    None
)

VERSION_OPTION = (
    "version",
    ord("V"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.NONE,
    "show version",
    None
)


class DeltaCommandLine(DeltaEntity):

    def _set_options(self, application):
        application.add_main_option(*INIT_OPTION)
        application.add_main_option(*VERSION_OPTION)

    def _on_command_line(self, application, command_line):
        options = command_line.get_options_dict()
        if options.lookup_value("version"):
            print("version :", VERSION)
        else:
            initialize_only = options.lookup_value("init") is not None
            self._raise("delta > run", initialize_only)
        return 0

    def __init__(self, parent):
        self._parent = parent
        application = self._enquiry("delta > application")
        self._set_options(application)
        application.connect("command-line", self._on_command_line)
