# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako_launcher import APPLICATION_ID
from kusozako_launcher.Entity import DeltaEntity
from .window.Window import DeltaWindow
from .watchers.Watchers import EchoWatchers
from .app_info_models.AppInfoModels import DeltaAppInfoModels


class DeltaApplication(Gtk.Application, DeltaEntity):

    def _delta_call_filter_changed(self, keyword):
        self._app_info_models.change_filter(keyword)

    def _delta_info_app_info_model(self):
        return self._app_info_models.get_filter_model()

    def _delta_call_run(self, initialize_only):
        self._try_activate()
        if not initialize_only and not self.get_windows():
            DeltaWindow(self)

    def _try_activate(self):
        if self._app_info_models is None:
            self._app_info_models = DeltaAppInfoModels(self)
            self.activate()

    def _delta_info_application(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        self._app_info_models = None
        Gtk.Application.__init__(
            self,
            application_id=APPLICATION_ID,
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            )
        EchoWatchers(self)
