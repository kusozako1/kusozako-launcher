# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SoundOnShow import DeltaSoundOnShow
from .KeyReleased import DeltaKeyReleased
from .LayerShellSetupOnRealize import DeltaLayerShellSetupOnRealize
from .Destroy import DeltaDestroy


class EchoWatchers:

    def __init__(self, parent):
        DeltaSoundOnShow(parent)
        DeltaKeyReleased(parent)
        DeltaLayerShellSetupOnRealize(parent)
        DeltaDestroy(parent)
