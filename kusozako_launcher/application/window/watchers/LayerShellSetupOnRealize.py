# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkLayerShell
from kusozako_bar.Entity import DeltaEntity
from kusozako_launcher.config.Config import KusozakoConfig


class DeltaLayerShellSetupOnRealize(DeltaEntity):

    def _on_realize(self, window):
        GtkLayerShell.init_for_window(window)
        GtkLayerShell.set_keyboard_mode(
            window,
            GtkLayerShell.KeyboardMode.EXCLUSIVE,
            )
        GtkLayerShell.set_layer(window, GtkLayerShell.Layer.OVERLAY)
        config = KusozakoConfig.get_default()
        window.set_size_request(config["width"], config["height"])

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        window.connect("realize", self._on_realize)
