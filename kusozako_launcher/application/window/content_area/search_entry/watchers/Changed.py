# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class DeltaChanged(DeltaEntity):

    def _on_event(self, entry):
        self._raise("delta > extend timeout")
        self._raise("delta > filter changed", entry.get_text())

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("changed", self._on_event)
