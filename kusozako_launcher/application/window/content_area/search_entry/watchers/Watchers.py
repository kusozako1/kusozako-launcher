# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Show import DeltaShow
from .Changed import DeltaChanged
from .Activate import DeltaActivate


class EchoWatchers:

    def __init__(self, parent):
        DeltaShow(parent)
        DeltaChanged(parent)
        DeltaActivate(parent)
