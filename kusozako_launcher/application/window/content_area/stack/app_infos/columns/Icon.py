# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity


class DeltaIcon(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, iter_, user_data=None):
        gicon = model[iter_][0]
        if gicon:
            renderer.set_property("gicon", gicon)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        renderer = Gtk.CellRendererPixbuf.new()
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            )
        self.set_sort_column_id(1)
        self.set_cell_data_func(renderer, self._cell_data_func)
        tree_view.append_column(self)
