# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Realize import DeltaRealize
from .RowActivated import DeltaRowActivated
from .CursorChanged import DeltaCursorChanged
from .RowInsertedOrDeleted import DeltaRowInsertedOrDeleted


class EchoWatchers:

    def __init__(self, parent):
        DeltaRealize(parent)
        DeltaRowActivated(parent)
        DeltaCursorChanged(parent)
        DeltaRowInsertedOrDeleted(parent)
