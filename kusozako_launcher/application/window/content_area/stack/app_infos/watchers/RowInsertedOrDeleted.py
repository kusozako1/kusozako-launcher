# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity


class DeltaRowInsertedOrDeleted(DeltaEntity):

    def _set_cursor(self, tree_selection):
        model, selected_iter = tree_selection.get_selected()
        if selected_iter is None:
            tree_path = Gtk.TreePath.new_from_string("0")
            tree_selection.select_path(tree_path)
        elif selected_iter is not None:
            tree_path = model.get_path(selected_iter)
        self._raise("delta > set cursor", tree_path)

    def _on_event(self, model, path, _, tree_selection):
        if len(model) == 0:
            return
        self._set_cursor(tree_selection)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        selection = tree_view.get_selection()
        model = tree_view.get_model()
        model.connect("row-inserted", self._on_event, selection)
        model.connect("row-deleted", self._on_event, "dummy", selection)
