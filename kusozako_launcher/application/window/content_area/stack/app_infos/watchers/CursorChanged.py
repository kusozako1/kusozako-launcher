# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class DeltaCursorChanged(DeltaEntity):

    def _on_event(self, tree_view):
        self._raise("delta > extend timeout")

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("cursor-changed", self._on_event)
