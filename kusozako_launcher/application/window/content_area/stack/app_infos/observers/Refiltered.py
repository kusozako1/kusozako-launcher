# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.const import LauncherSignals
from kusozako_launcher.alfa.LauncherObserver import AlfaLauncherObserver


class DeltaRefitered(AlfaLauncherObserver):

    __signal__ = LauncherSignals.REFILTERED

    def _dispatch(self, tree_view):
        model, selected_iter = self._tree_selection.get_selected()
        if selected_iter is None:
            tree_path = Gtk.TreePath.new_from_string("0")
            self._tree_selection.select_path(tree_path)
        elif selected_iter is not None:
            tree_path = model.get_path(selected_iter)
        tree_view.set_cursor(tree_path, None, False)

    def _signal_received(self, param=None):
        tree_view = self._enquiry("delta > tree view")
        model = tree_view.get_model()
        if model is None or len(model) == 0:
            return
        self._dispatch(tree_view)

    def _on_initialize(self):
        tree_view = self._enquiry("delta > tree view")
        self._tree_selection = tree_view.get_selection()
