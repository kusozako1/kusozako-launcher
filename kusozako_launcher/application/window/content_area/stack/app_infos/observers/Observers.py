# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .DownKeyPressed import DeltaDownKeyPressed
from .UpKeyPressed import DeltaUpKeyPressed
from .entry_activated.EntryActivated import DeltaEntryActivated


class EchoObservers:

    def __init__(self, parent):
        DeltaDownKeyPressed(parent)
        DeltaUpKeyPressed(parent)
        DeltaEntryActivated(parent)
