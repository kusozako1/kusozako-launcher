# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import LauncherSignals


class DeltaUpKeyPressed(DeltaEntity):

    def _signal_received(self, param=None):
        tree_view = self._enquiry("delta > tree view")
        tree_selection = tree_view.get_selection()
        if tree_selection is None:
            return
        model, selected_iter = tree_selection.get_selected()
        if selected_iter is None:
            return
        selected_path = model.get_path(selected_iter)
        selected_path.prev()
        tree_selection.select_path(selected_path)
        tree_view.set_cursor(selected_path, None, False)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == LauncherSignals.UP_KEY_PRESSED:
            self._signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register launcher object", self)
