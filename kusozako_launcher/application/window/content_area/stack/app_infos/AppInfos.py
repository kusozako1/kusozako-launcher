# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity
from .watchers.Watchers import EchoWatchers
from .observers.Observers import EchoObservers
from .columns.Columns import EchoColumns


class DeltaAppInfos(Gtk.TreeView, DeltaEntity):

    def _delta_info_tree_view(self):
        return self

    def _delta_call_set_cursor(self, tree_path):
        self.set_cursor(tree_path, None, False)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.TreeView.__init__(
            self,
            model=self._enquiry("delta > app info model"),
            headers_visible=False,
            )
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        EchoObservers(self)
        EchoWatchers(self)
        EchoColumns(self)
        scrolled_window.add(self)
        self._raise("delta > add to container", scrolled_window)
