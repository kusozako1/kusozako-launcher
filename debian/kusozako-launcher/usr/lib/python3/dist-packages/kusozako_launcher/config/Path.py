# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_launcher import APPLICATION_ID

_CONFIG_DIR = GLib.get_user_config_dir()
_DIRECTORY_NAMES = [_CONFIG_DIR, APPLICATION_ID]
DIRECTORY_PATH = GLib.build_filenamev(_DIRECTORY_NAMES)
_FILE_NAMES = [_CONFIG_DIR, APPLICATION_ID, "config.json"]
FILE_PATH = GLib.build_filenamev(_FILE_NAMES)
