# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_launcher.const import ConfigKeys
from kusozako_launcher.config.Config import KusozakoConfig

FORMAT = 'background-image: url("{}");'


class KusozakoBackgroundImage:

    def _get_path(self):
        config = KusozakoConfig.get_default()
        path = config[ConfigKeys.BACKGROUND_IMAGE]
        if path is None:
            return None
        if path.startswith("~/"):
            path = path.replace("~", GLib.get_home_dir(), 1)
        return path

    def _path_to_uri(self, path):
        gfile = Gio.File.new_for_path(path)
        if not gfile.query_exists():
            return None
        return gfile.get_uri()

    def replace(self, raw_data):
        path = self._get_path()
        if path is None:
            return raw_data
        uri = self._path_to_uri(path)
        if uri is None:
            return raw_data
        replacement = FORMAT.format(uri)
        return raw_data.replace("/*KUSOZAKO_BACKGROUND_IMAGE*/", replacement)
