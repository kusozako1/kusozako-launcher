# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import LauncherSignals


class DeltaAppInfoLauncher(DeltaEntity):

    def _launch_in_terminal(self, app_info):
        command_line = app_info.get_commandline()
        if command_line is None:
            return
        subprocess = Gio.Subprocess.new(
            ["kusozako-console", "-x", command_line],
            Gio.SubprocessFlags.NONE
            )
        subprocess.wait_async(None, None, None)

    def _launch_with_context(self, app_info):
        context = Gio.AppLaunchContext.new()
        app_info.launch(None, context)

    def launch(self, app_info):
        if app_info.get_boolean("Terminal"):
            self._launch_in_terminal(app_info)
        else:
            self._launch_with_context(app_info)
        user_data = LauncherSignals.APP_INFO_LAUNCHED, app_info
        self._raise("delta > launcher signal", user_data)
        self._raise("delta > close")

    def __init__(self, parent):
        self._parent = parent
