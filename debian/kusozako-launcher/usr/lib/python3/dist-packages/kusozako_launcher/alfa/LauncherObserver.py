# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class AlfaLauncherObserver(DeltaEntity):

    __signal__ = "define receiving signal here."

    def _signal_received(self, param=None):
        raise NotImplementedError

    def _on_initialize(self):
        pass

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.__signal__:
            self._signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise("delta > register launcher object", self)
