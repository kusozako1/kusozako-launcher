# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class DeltaShow(DeltaEntity):

    def _on_event(self, entry):
        application_window = self._enquiry("delta > application window")
        application_window.set_focus(entry)

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("show", self._on_event)
