# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity
from .app_infos.AppInfos import DeltaAppInfos


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaAppInfos(self)
        self._raise("delta > add to container", self)
