# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import AppInfoColumns
from kusozako_launcher.util.AppInfoLauncher import DeltaAppInfoLauncher


class DeltaRowActivated(DeltaEntity):

    def _on_event(self, tree_view, path, column):
        model = tree_view.get_model()
        app_info = model[path][AppInfoColumns.APP_INFO]
        self._app_info_launcher.launch(app_info)

    def __init__(self, parent):
        self._parent = parent
        self._app_info_launcher = DeltaAppInfoLauncher(self)
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("row-activated", self._on_event)
