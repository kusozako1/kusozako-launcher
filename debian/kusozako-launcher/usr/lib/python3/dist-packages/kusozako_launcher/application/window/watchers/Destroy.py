# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity


class DeltaDestroy(DeltaEntity):

    def _on_event(self, window):
        self._raise("delta > filter changed", "")

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        window.connect("destroy", self._on_event)
