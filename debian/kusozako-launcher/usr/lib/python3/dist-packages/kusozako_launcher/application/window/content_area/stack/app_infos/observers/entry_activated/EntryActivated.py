# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.const import LauncherSignals
from kusozako_launcher.alfa.LauncherObserver import AlfaLauncherObserver
from kusozako_launcher.const import AppInfoColumns
from kusozako_launcher.util.AppInfoLauncher import DeltaAppInfoLauncher
from .Subprocess import DeltaSubprocess


class DeltaEntryActivated(AlfaLauncherObserver):

    __signal__ = LauncherSignals.ENTRY_ACTIVATED

    def _signal_received(self, param):
        model, selected_iter = self._tree_selection.get_selected()
        if selected_iter is None:
            self._subprocess.try_execute(param)
        else:
            app_info = model[selected_iter][AppInfoColumns.APP_INFO]
            self._app_info_launcher.launch(app_info)

    def _on_initialize(self):
        self._app_info_launcher = DeltaAppInfoLauncher(self)
        tree_view = self._enquiry("delta > tree view")
        self._tree_selection = tree_view.get_selection()
        self._subprocess = DeltaSubprocess(self)
