# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity


class DeltaRealize(DeltaEntity):

    def _on_event(self, tree_view):
        tree_path = Gtk.TreePath.new_from_string("0")
        tree_selection = tree_view.get_selection()
        tree_selection.select_path(tree_path)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("realize", self._on_event)
