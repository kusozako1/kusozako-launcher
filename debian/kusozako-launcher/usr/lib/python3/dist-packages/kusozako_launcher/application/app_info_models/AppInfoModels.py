# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .filter_model.FilterModel import DeltaFilterModel


class DeltaAppInfoModels(DeltaEntity):

    def get_filter_model(self):
        return self._filter_model.get_model()

    def change_filter(self, keyword):
        self._filter_model.set_keyword(keyword)

    def __init__(self, parent):
        self._parent = parent
        base_model = DeltaBaseModel(self)
        self._filter_model = DeltaFilterModel.new(self, base_model)
