# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import AppInfoColumns


class DeltaFilterModel(DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _visible_func(self, model, iter_, user_data=None):
        if not self._keyword:
            return True
        keywords = model[iter_][AppInfoColumns.KEYWORDS]
        return self._keyword in GLib.utf8_strdown(keywords, -1)

    def set_keyword(self, keyword):
        self._keyword = GLib.utf8_strdown(keyword, -1)
        self._filter_model.refilter()

    def get_model(self):
        return self._filter_model

    def construct(self, model):
        self._filter_model = model.filter_new(None)
        self._filter_model.set_visible_func(self._visible_func)

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
