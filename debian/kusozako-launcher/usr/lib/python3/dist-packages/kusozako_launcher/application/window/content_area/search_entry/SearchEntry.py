# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity
from .EventBox import DeltaEventBox
from .watchers.Watchers import EchoWatchers


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _delta_info_entry(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        event_box = DeltaEventBox(self)
        Gtk.SearchEntry.__init__(self)
        EchoWatchers(self)
        event_box.add(self)
        self._raise("delta > add to container", event_box)
