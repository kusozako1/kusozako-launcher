# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import LauncherSignals

SIGNALS = {
    65364: LauncherSignals.DOWN_KEY_PRESSED,
    65362: LauncherSignals.UP_KEY_PRESSED,
}


class DeltaEventBox(Gtk.EventBox, DeltaEntity):

    def _on_key_press_event(self, event_box, event_key):
        signal = SIGNALS.get(event_key.keyval, None)
        if signal is None:
            return Gdk.EVENT_PROPAGATE
        user_data = signal, None
        self._raise("delta > launcher signal", user_data)
        return Gdk.EVENT_STOP

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self.connect("key-press-event", self._on_key_press_event)
