# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_launcher.Entity import DeltaEntity
from .Path import DeltaPath
from .observers.Observers import EchoObservers


class DeltaPriorityCache(DeltaEntity):

    def _delta_call_path_initialized(self, path):
        self._key_file = GLib.KeyFile.new()
        self._key_file.load_from_file(path, GLib.KeyFileFlags.NONE)

    def _delta_call_app_info_launched(self, app_info):
        name = app_info.get_name()
        current_priority = self.get_priority(name)
        key = name.replace(" ", "_")
        priority = current_priority+1
        self._key_file.set_integer("priority", key, priority)
        target_path = self._path.get_target_path()
        self._key_file.save_to_file(target_path)

    def get_priority(self, name):
        key = name.replace(" ", "_")
        try:
            return self._key_file.get_integer("priority", key)
        except GLib.Error:
            return 0

    def __init__(self, parent):
        self._parent = parent
        self._path = DeltaPath(self)
        EchoObservers(self)
