# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.const import LauncherSignals


class DeltaActivate(DeltaEntity):

    def _on_event(self, entry):
        user_data = LauncherSignals.ENTRY_ACTIVATED, entry.get_text()
        self._raise("delta > launcher signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("activate", self._on_event)
