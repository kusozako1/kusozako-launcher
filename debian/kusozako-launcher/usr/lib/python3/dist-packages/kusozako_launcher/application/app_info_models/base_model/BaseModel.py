# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_launcher.Entity import DeltaEntity
from .builder.Builder import DeltaBuilder
from .Sort import FoxtrotSort


class DeltaBaseModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append(self, row_data):
        self.append(row_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self)
        self.set_column_types([Gio.Icon, str, Gio.AppInfo, str, int])
        FoxtrotSort(self)
        DeltaBuilder(self)
