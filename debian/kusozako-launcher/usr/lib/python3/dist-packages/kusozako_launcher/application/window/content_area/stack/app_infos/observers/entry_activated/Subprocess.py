# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_launcher.Entity import DeltaEntity


class DeltaSubprocess(DeltaEntity):

    def _finished(self, subprocess, task, command):
        successful, stdout, stderr = subprocess.communicate_finish(task)
        self._raise("delta > close")

    def _try_run_command(self, command):
        try:
            subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
            subprocess.communicate_async(None, None, self._finished, command)
        except GLib.Error as error_:
            print("error !!", error_.message)

    def try_execute(self, param):
        if not param:
            return
        command = param.split()
        self._try_run_command(command)

    def __init__(self, parent):
        self._parent = parent
