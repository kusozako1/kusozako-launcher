# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_launcher import APPLICATION_ID
from kusozako_launcher.Entity import DeltaEntity

NAMES = [GLib.get_user_cache_dir(), APPLICATION_ID]
DIRECTORY = GLib.build_filenamev(NAMES)


class DeltaPath(DeltaEntity):

    def _initialize(self):
        if self._gfile.query_exists():
            path = self._gfile.get_path()
        else:
            names = [GLib.path_get_dirname(__file__), "priority.conf"]
            path = GLib.build_filenamev(names)
        self._raise("delta > path initialized", path)

    def get_target_path(self):
        return self._gfile.get_path()

    def __init__(self, parent):
        self._parent = parent
        directory_gfile = Gio.File.new_for_path(DIRECTORY)
        if not directory_gfile.query_exists(None):
            directory_gfile.make_directory(None)
        self._gfile = directory_gfile.get_child("priority.conf")
        self._initialize()
