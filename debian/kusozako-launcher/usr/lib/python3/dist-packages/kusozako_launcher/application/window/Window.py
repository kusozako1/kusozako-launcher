# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity
from kusozako_launcher.Transmitter import FoxtrotTransmitter
from .watchers.Watchers import EchoWatchers
from .close_timeout.CloseTimeout import DeltaCloseTimeout
from .content_area.ContentArea import DeltaContentArea


class DeltaWindow(Gtk.Window, DeltaEntity):

    def _delta_call_launcher_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_launcher_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_info_application_window(self):
        return self

    def _delta_call_extend_timeout(self):
        self._close_timeout.extend()

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_close(self):
        self.close()

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Gtk.Window.__init__(self)
        self._close_timeout = DeltaCloseTimeout(self)
        EchoWatchers(self)
        DeltaContentArea(self)
        self.show_all()
