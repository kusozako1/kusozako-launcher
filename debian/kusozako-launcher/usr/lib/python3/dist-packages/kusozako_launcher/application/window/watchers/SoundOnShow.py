# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GSound
from kusozako_launcher.Entity import DeltaEntity


class DeltaSoundOnShow(DeltaEntity):

    def _on_event(self, window, sound_context):
        sound_context.play_simple({GSound.ATTR_EVENT_ID: "dialog-warning"})

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        sound_context = GSound.Context.new()
        window.connect("show", self._on_event, sound_context)
