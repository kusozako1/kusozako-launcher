# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity


class DeltaMessageArea(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "press escape to close", xalign=1, yalign=1)
        self.set_size_request(-1, 200)
        self._raise("delta > add to container", self)
