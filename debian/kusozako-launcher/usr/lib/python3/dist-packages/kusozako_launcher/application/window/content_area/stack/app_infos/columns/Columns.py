# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Icon import DeltaIcon
from .Description import DeltaDescription


class EchoColumns:

    def __init__(self, parent):
        DeltaIcon(parent)
        DeltaDescription(parent)
