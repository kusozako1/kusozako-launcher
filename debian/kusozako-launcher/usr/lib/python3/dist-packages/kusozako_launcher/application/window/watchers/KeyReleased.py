# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako_launcher.Entity import DeltaEntity


class DeltaKeyReleased(DeltaEntity):

    def _on_key_release(self, window, event_key):
        _, keycode = event_key.get_keycode()
        if keycode == 9:
            window.close()

    def _on_event(self, window):
        gdk_window = window.get_window()
        gdk_window.set_events(Gdk.EventMask.KEY_RELEASE_MASK)
        window.connect("key-release-event", self._on_key_release)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        window.connect("realize", self._on_event)
