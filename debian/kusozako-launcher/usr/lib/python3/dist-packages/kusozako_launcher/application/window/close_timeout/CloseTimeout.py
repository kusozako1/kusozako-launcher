# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity


class DeltaCloseTimeout(DeltaEntity):

    def _timeout(self, window):
        self._count -= 1
        if 0 >= self._count:
            window.close()
            return GLib.SOURCE_REMOVE
        else:
            return GLib.SOURCE_CONTINUE

    def _on_show(self, window):
        GLib.timeout_add_seconds(1, self._timeout, window)

    def _on_close(self, window):
        self._count = 0

    def extend(self):
        self._count = min(20, self._count+10)

    def __init__(self, parent):
        self._parent = parent
        self._count = 10
        window = self._enquiry("delta > application window")
        window.connect("show", self._on_show)
        window.connect("destroy", self._on_close)
