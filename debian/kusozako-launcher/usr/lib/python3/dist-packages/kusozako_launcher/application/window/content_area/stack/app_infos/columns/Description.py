# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity


class DeltaDescription(Gtk.TreeViewColumn, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        renderer = Gtk.CellRendererText.new()
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            text=1,
            )
        self.set_sort_column_id(1)
        tree_view.append_column(self)
