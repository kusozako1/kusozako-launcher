# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_launcher.Entity import DeltaEntity
from kusozako_launcher.config.Config import KusozakoConfig
from .message_area.MessageArea import DeltaMessageArea
from .search_entry.SearchEntry import DeltaSearchEntry
from .stack.Stack import DeltaStack


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            vexpand=True,
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            spacing=8,
            )
        config = KusozakoConfig.get_default()
        self.set_opacity(config["opacity"])
        DeltaMessageArea(self)
        DeltaSearchEntry(self)
        DeltaStack(self)
        self._raise("delta > add to container", self)
