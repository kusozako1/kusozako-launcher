# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

WIDTH = "width"
HEIGHT = "height"
BACKGROUND_COLOR = "background-color"
FOREGROUND_COLOR = "foreground-color"
BACKGROUND_IMAGE = "background-image"
