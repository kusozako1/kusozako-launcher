#!/usr/bin/env python3

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_launcher.MainLoop import DeltaMainLoop


if __name__ == "__main__":
    DeltaMainLoop()
